# sambowler.co.uk

Source code for https://sambowler.co.uk.

1. `yarn install`
2. `gulp` or `gulp build`

## Server

`npm run server:start`
