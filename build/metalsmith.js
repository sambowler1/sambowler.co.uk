const Metalsmith = require('metalsmith')
const discoverPartials = require('metalsmith-discover-partials');
const layouts = require('metalsmith-layouts')
const collections = require('metalsmith-collections')
const inPlace = require('metalsmith-in-place')
const permalinks = require('metalsmith-permalinks')
const Handlebars = require('handlebars')

const PROJECTS_REGEX = /projects\/.+/

Handlebars.registerHelper('isCurrentPage', (url, options) => {
  const path = options.data.root.path
  let isCurrent = url === path

  // If in projects section
  if (path.match(PROJECTS_REGEX) !== null && url === 'projects') {
    isCurrent = true
  }

  return isCurrent ? options.fn(options.data.root) : options.inverse(options.data.root)
})

// Handlebars.registerHelper('isProject', (options) => {
//   const isProject = options.data.root.path.match(PROJECTS_REGEX) !== null
//
//   return isProject ? options.fn(options.data.root) : options.inverse(options.data.root)
// })

Handlebars.registerHelper('isHome', (options) => {
  const isHome = options.data.root.path === ''

  return isHome ? options.fn(options.data.root) : options.inverse(options.data.root)
});

Metalsmith(`${__dirname}/../client/src/`)
  .metadata({
    copyrightDate: new Date().getFullYear(),
    availability: {
      available: true,
      text: 'Accepting new work from Sept. 2017'
    },
    nav: [{
        title: 'Home',
        url: '',
        icon: 'home'
      },
      {
        title: 'Projects',
        url: 'projects',
        icon: 'briefcase'
      },
      {
        title: 'About',
        url: 'about',
        icon: 'user'
      }
    ]
  })
  .source(`${__dirname}/../client/src/pages`)
  .destination(`${__dirname}/../dist`)
  .use(collections({
    projects: {
      pattern: 'projects/*.hbs',
      sortBy: 'position'
    }
  }))
  /**
   * .hbs file extensions seem to be retained in the collections data. We need
   * to remove it to tie in with permalinks() above
   */
  .use((_files, metalsmith, done) => {
    let projects = metalsmith.metadata().collections.projects;

    for (let project of projects) {
      project.path = project.path.replace(/.hbs$/, '');
    }

    done();
  })
  .use(inPlace())
  .use(permalinks())
  .use(discoverPartials({
    directory: `${__dirname}/../client/src/partials`
  }))
  .use(layouts())
  .build(err => {
    if (err) throw err;
  });
