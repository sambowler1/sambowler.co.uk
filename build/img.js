const sharp = require('sharp');

const widthsMap = {
  'my-head.jpg': [120, 150, 160, 180, 200, 240, 300, 320, 360, 400],
  'uk-radio-player-interface.jpg': [250, 350, 450, 500, 550, 700, 900]
}

Object.keys(widthsMap).forEach(async (imgFile) => {
  const imgFilePath = `${__dirname}/../client/img/${imgFile}`;
  const pipeline = sharp(imgFilePath);
  const widths = widthsMap[imgFile];

  widths.forEach((width) => {
    const outputFile = `${__dirname}/../dist/img/${imgFile.split('.')[0]}-${width}.${imgFile.split('.')[1]}`;

    pipeline
      .resize(width)
      .toFile(outputFile)
      .catch(() => {
        throw new Error(`Unable to resize ${imgFile}`);
      });
  });
});
