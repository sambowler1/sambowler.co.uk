const express = require('express');
const formidable = require('express-formidable');
const Joi = require('joi')
const http = require('http');
const bodyParser = require('body-parser');
const favicon = require('serve-favicon');
const Mailgun = require('mailgun').Mailgun;
const { EMAIL_RECIPIENT, MAILGUN_API_KEY, MAILGUN_TIMEOUT } = require('../config');

if(!MAILGUN_API_KEY) throw new Error('Please set a Mailgun API key.');
if(!EMAIL_RECIPIENT) throw new Error('Please set an email recipient');

const mg = new Mailgun(MAILGUN_API_KEY);
const app = express();
const contactValidationSchema = Joi.object().keys({
  name: Joi.string().allow(''),
  email: Joi.string().email().required(),
  message: Joi.string().required()
})

const PORT = 8080;

app.use(express.static(`${process.cwd()}/dist`));
app.use(favicon(`${process.cwd()}/dist/favicon.ico`));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(formidable())

function sendEmail(name, email, message) {
  console.log('Sending email:', name, email);

  return new Promise((resolve, reject) => {
    let subject = `Contact form: ${name}, ${email}`;

    mg.sendText('hello@sambowler.co.uk', EMAIL_RECIPIENT, subject, message, (err) => {
      if(err) {
        reject();
      }

      console.log('Email sent:', name, email);

      resolve();
    });

    setTimeout(function() {
      reject();
    }, MAILGUN_TIMEOUT || 5000);
  });
}

app.post('/contact', (req, res) => {
  const { error } = Joi.validate(req.fields, contactValidationSchema)

  if(error === null) {
    sendEmail(req.fields.name || '', req.fields.email, req.fields.message)
      .then(() => {
        res.json({
          success: true
        });
      })
      .catch(() => {
        res.json({
          success: false
        });
      });
  } else {
    res.status(400)

    res.json({
      success: false
    })
  }
});

app.get('/projects', (req, res) => {
  res.status(301);
  res.redirect('/');
})

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
