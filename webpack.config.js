const path = require('path');

module.exports = {
  entry: './client/js/index.js',
  output: {
    path: path.resolve(__dirname, 'dist', 'js'),
    // publicPath: './dist/js',
    filename: 'bundle.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      options: {
        presets: ['@babel/preset-env']
      }
    }]
  }
};
