const autoprefixer = require('autoprefixer');
const postcssSvg = require('postcss-svg');

module.exports = {
    plugins: [
        autoprefixer(),
        postcssSvg({
            dirs: ['./client']
        })
    ]
};
