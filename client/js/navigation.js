class Navigation {
  constructor() {
    this.SUPPORTS_ANIMS = 'animate' in document.createElement('div')
    this.TIMEOUT_DELAY = 3000
    this.LOADER_ANIM_DURATION = 200
    this.LOADER_ANIM_EASING = 'ease-out'

    this.currLoaderProgress = 0

    document.body.addEventListener('click', (e) => {
      if(this.isLink(e.target) && this.isInternalLink(e.target)) {
        e.preventDefault()

        // If not trying to load the current page
        if(e.target.pathname !== document.location.pathname) {
          this.goTo(e.target.pathname)
        }
      }
    })

    window.addEventListener('popstate', (e) => {
      this.goTo(document.location.pathname, false)
    })

    this.initLoader()
  }

  initLoader() {
    const loader = document.createElement('div')

    loader.classList.add('page-loader')

    this.loader = loader

    document.body.appendChild(loader)
  }

  setProgress(progress) {
    return new Promise((resolve, reject) => {
      if(this.SUPPORTS_ANIMS) {
        this.animateProgress(progress).then(resolve)
      } else {
        this.loader.style.width = `${progress}%`
        this.loader.style.opacity = 1

        /**
         * If we're animating to the end keep the loading indicator present
         * so it's clear to the user that loading has happened and the page
         * has changed.
         */
        if(progress === 100) {
          setTimeout(() => {
            this.loader.style.opacity = 0
          }, 500)
        }

        this.currLoaderProgress = progress

        resolve()
      }
    })
  }

  animateProgress(progress) {
    return new Promise((resolve, reject) => {
      let keyframes = []
      let startFrame = { width: `${this.currLoaderProgress}%` }
      let endFrame = { width: `${progress}%` }

      // If currently at 0 progress, fade in
      if(this.currLoaderProgress === 0) {
        startFrame.opacity = 0
        endFrame.opacity = 1
      }

      // If animating to 100% progress, fade out
      if(progress === 100) {
        startFrame.opacity = 1
        endFrame.opacity = 0
      }

      keyframes.push(startFrame)
      keyframes.push(endFrame)

      const animation = this.loader.animate(keyframes, {
        duration: this.LOADER_ANIM_DURATION,
        easing: this.LOADER_ANIM_EASING
      })

      animation.onfinish = () => {
        if(progress !== 100) {
          this.loader.style.opacity = 1
          this.loader.style.width = `${progress}%`

          this.currLoaderProgress = progress
        } else {
          this.loader.style.opacity = 0
          this.loader.style.width = 0

          // Reset to the start
          this.currLoaderProgress = 0
        }

        resolve()
      }
    })
  }

  isLink(evtTarget) {
    return evtTarget.nodeName.toLowerCase() === 'a'
  }

  isInternalLink(link) {
    return link.host === window.location.host
  }

  goTo(pathname, pushState) {
    pushState = typeof pushState !== 'undefined' ? pushState : true

    // Ensure the page doesn't load before the first animation has completed
    Promise.all([ this.setProgress(20), this.loadPage(pathname, pushState) ])
      .then((results) => {
        this.updatePageContent.call(this, results[1])

        // Trigger an event when the page changes
        document.dispatchEvent(new CustomEvent('page-change', { detail: { pathname }}))

        this.setProgress(100)
      })
      .catch((err) => {
        // Fall back to traditional navigation
        window.location.pathname = pathname
      })
  }

  loadPage(pathname, pushState) {
    return new Promise((resolve, reject) => {
      const timeout = setTimeout(reject, this.TIMEOUT_DELAY)

      fetch(pathname)
        .then((resp) => {
          clearTimeout(timeout)

          if(resp.status !== 200) {
            reject(`Error getting that page: bad HTTP code (${resp.status})`)
          }

          resp.text().then((pageContent) => {
            const newHtml = document.createElement('html')

            newHtml.innerHTML = pageContent

            const newPage = newHtml.querySelector('.page-wrapper')
            const newTitle = newHtml.querySelector('title').innerText
            const bodyClass = newHtml.querySelector('body').className

            document.title = newTitle

            /**
             * We only want to push state if we're going forwards.
             * If the user has used the back or forwards button we don't need to.
             */
            if(pushState) history.pushState({}, newTitle, pathname)

            resolve({ pathname, newPage, bodyClass })
          })
        })
        .catch((err) => {
          clearTimeout(timeout)

          reject(`Error getting that page: ${err}`)
        })
    })
  }

  updatePageContent(resp) {
    const { pathname, newPage, bodyClass } = resp
    const currPage = document.querySelector('.page-wrapper')

    currPage.parentNode.replaceChild(newPage, currPage)

    // Make sure page-level classes are transferred as well
    document.body.className = bodyClass

    window.scrollTo(0, 0)
  }
}

export default Navigation
