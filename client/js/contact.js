class ContactForm {
  constructor() {
    this.SHOW_CONTACT_CLASS = 'js-show-contact'
    // Duration to wait after form submission before resetting the fields
    this.RESET_DELAY = 3000

    this.pageWrapper = document.querySelector('.page-wrapper')
    this.contactFormWrapper = document.querySelector('.js-contact')

    if(!this.contactFormWrapper) return; // No contact form on the page

    this.contactForm = this.contactFormWrapper.querySelector('form')
    this.closeContact = document.querySelectorAll('.js-close-contact')

    this._handleBodyClick = this.handleBodyClick.bind(this)
    this._handleKeydown = this.handleKeydown.bind(this)

    this.name = document.getElementById('name')
    this.email = document.getElementById('email')
    this.message = document.getElementById('message')
    this.sendButton = document.querySelector('.submit-contact-form')

    this.attachEventListeners()
  }

  attachEventListeners() {
    // Delegate click to body
    document.body.addEventListener('click', (e) => {
      if(this.isShowContactBtn(e.target)) {
        e.preventDefault()
        e.stopPropagation()

        this.clickedElement = e.target.closest(`.${this.SHOW_CONTACT_CLASS}`)

        this.show()
      }
    })

    Array.from(this.closeContact).forEach((closeContactBtn) => {
      closeContactBtn.addEventListener('click', () => this.hide())
    })

    // Check form validity whenever a key is released so we can enable/disable the submission button
    this.contactForm.addEventListener('keyup', (e) => {
      this.contactForm.checkValidity() ? this.formBecameValid() : this.formBecameInvalid()
    })

    this.contactForm.addEventListener('submit', (e) => {
      e.preventDefault()

      const EXCLUSION_CLASSES = ['is-sending', 'is-sent']
      // Condition is satisfied if none of EXCLUSION_CLASSES are contained in
      // this.sendButton's classList
      const sendButtonIsValid = EXCLUSION_CLASSES.every((klass) => {
        return !this.sendButton.classList.contains(klass)
      })

      if(this.contactForm.checkValidity() && sendButtonIsValid) {
        this.sendButton.classList.remove('is-errored');
        this.sendButton.classList.add('is-sending');
        this.sendButton.setAttribute('disabled', '');

        this.sendButton.querySelector('.submit-contact-form__text').innerText = 'Sending...';

        this.submitForm()
          .then(() => {
            this.sendButton.classList.remove('is-sending');
            this.sendButton.classList.add('is-sent');

            this.sendButton.querySelector('.submit-contact-form__text').innerText = 'Sent! Thank you';

            setTimeout(this.resetForm.bind(this), this.RESET_DELAY)
          })
          .catch(() => {
            this.sendButton.classList.remove('is-sending');
            this.sendButton.classList.add('is-errored');
            this.sendButton.removeAttribute('disabled', '');

            this.sendButton.querySelector('.submit-contact-form__text').innerText = 'There was a problem. Try again?';
          })
      }
    });
  }

  // Checks whether "el" is a child of an element which should show the contact form
  isShowContactBtn(el) {
    return el !== null && el.closest(`.${this.SHOW_CONTACT_CLASS}`) !== null
  }

  // Close the form if the user clicks on the blank space outside
  handleBodyClick(e) {
    // If not in contact form
    if(e.target.closest('.contact') === null) {
      this.hide()
    }
  }

  // Close on esc press
  handleKeydown(e) {
    // 27 = ESC
    if(e.keyCode === 27) {
      e.preventDefault()

      this.hide()
    }
  }

  show() {
    document.body.classList.add('is-showing-modal')

    document.body.addEventListener('click', this._handleBodyClick)
    document.addEventListener('keydown', this._handleKeydown)

    this.pageWrapper.setAttribute('aria-hidden', 'true')
    this.contactFormWrapper.classList.add('is-visible')
    this.contactFormWrapper.removeAttribute('aria-hidden')
  }

  hide() {
    document.body.classList.remove('is-showing-modal')

    document.body.removeEventListener('click', this._handleBodyClick)
    document.removeEventListener('keydown', this._handleKeydown)

    this.pageWrapper.removeAttribute('aria-hidden')
    this.contactFormWrapper.classList.remove('is-visible')
    this.contactFormWrapper.setAttribute('aria-hidden', 'true')

    // Return focus to the elementhat triggered the form visibility
    if(this.clickedElement !== null) {
      this.clickedElement.focus()

      this.clickedElement = null
    }
  }

  formBecameValid() {
    this.contactForm.classList.add('is-valid');

    this.sendButton.removeAttribute('disabled');
  }

  formBecameInvalid() {
    this.contactForm.classList.remove('is-valid');

    this.sendButton.setAttribute('disabled', '');
  }

  // Reset all of the fields after submission
  resetForm() {
    this.name.value = ''
    this.email.value = ''
    this.message.value = ''

    this.sendButton.classList.remove('is-sending');
    this.sendButton.classList.remove('is-sent');
    this.sendButton.removeAttribute('disabled', '');

    this.sendButton.querySelector('.submit-contact-form__text').innerText = 'Send';
  }

  submitForm() {
    return new Promise((resolve, reject) => {
      const timeout = setTimeout(() => {
        reject();
      }, 5000);

      fetch('/contact', {
        method: 'post',
        body: new FormData(this.contactForm)
      })
        .then((resp) => {
          if(resp.status !== 200) {
            reject(`Error getting that page: bad HTTP code (${resp.status})`)
          }

          clearTimeout(timeout);

          resp.json().then((data) => {
            if(data.success) {
              resolve(data);
            } else {
              reject()
            }
          })
        })
        .catch(() => {
          clearTimeout(timeout);

          reject(Error('Network error'));
        })
    });
  }

  isOnline() {
    this.contactFormWrapper.classList.remove('is-offline')
  }

  isOffline() {
    this.contactFormWrapper.classList.add('is-offline')
  }
}

export default ContactForm;
