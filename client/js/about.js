import ExperienceList from './experience-list'

class About {
  constructor() {
    this.initExperienceLists()
  }

  static isAboutPage() {
    return document.querySelectorAll('.about').length > 0
  }

  initExperienceLists() {
    const experienceLists = document.querySelectorAll('.experience__list')

    if(experienceLists.length > 0) {
      Array.from(experienceLists).forEach((list) => {
        new ExperienceList(list)
      })
    }
  }
}

export default About
