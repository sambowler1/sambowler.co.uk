import Please from 'pleasejs/dist/Please.js'

class ExperienceList {
  constructor(list) {
    this.PLEASE_CONFIG = {
      hue: 93,
      saturation: 0.35,
      value: 0.69,
      format: 'rgb'
    }

    this.lis = list.querySelectorAll('li')

    this.lis.forEach((li) => {
      let color = Please.make_color(this.PLEASE_CONFIG)[0]

      li.style.backgroundColor = `rgba(${color.r}, ${color.g}, ${color.b}, 0.2)`
    })
  }
}

export default ExperienceList
