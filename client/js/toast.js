class Toast {
  constructor(text) {
    const existingToasts = document.querySelectorAll('.toast');

    if(existingToasts) {
      Array.from(existingToasts).forEach((toast) => {
        toast.remove();
      })
    }

    this.el = document.createElement('div');

    this.el.classList.add('toast');

    const innerDiv = document.createElement('div');

    innerDiv.classList.add('toast__inner');
    innerDiv.innerText = text;

    this.el.appendChild(innerDiv);

    document.body.appendChild(this.el);

    setTimeout(() => {
      this.el.addEventListener('animationend', () => {
        this.el.remove();
      });

      this.el.classList.add('is-hiding');
    }, 3000);
  }
}

export default Toast;
