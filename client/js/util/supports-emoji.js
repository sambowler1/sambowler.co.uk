// https://gist.github.com/mathisonian/7885295
export default () => {
  if(!document.createElement('canvas').getContext) return

  const context = document.createElement('canvas').getContext('2d')

  if(typeof context.fillText !== 'function') return

  const smiley = String.fromCharCode(55357) + String.fromCharCode(56835);

  context.textBaseline = 'top'
  context.font = '32px Arial'
  context.fillText(smiley, 0, 0)

  return context.getImageData(16, 16, 1, 1).data[0] !== 0
}
