require('es6-promise/auto')
require('whatwg-fetch')
require('element-closest')
require('core-js/fn/array/from')

import Navigation from './navigation'
import ContactForm from './contact'
import About from './about'

new Navigation()
const contactForm = new ContactForm()

import supportsEmoji from './util/supports-emoji'

if(About.isAboutPage()) {
  new About()
}

if(supportsEmoji()) {
  document.documentElement.classList.add('is-emoji-compatible')
}

window.addEventListener('online', () => {
  contactForm.isOnline()
})

window.addEventListener('offline', () => {
  contactForm.isOffline()
})

document.addEventListener('page-change', (e) => {
  if(e.detail.pathname === '/about' && About.isAboutPage()) {
    new About()
  }
})

if('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/sw.js');
}

const sourceCodeUrl = 'https://bitbucket.org/sambowler1/sambowler.co.uk'

console.info(`Hi! You can see the full source code here: ${sourceCodeUrl}\n\nIf you have any suggestions for improvements feel free to drop me an email at sam@sambowler.co.uk.`)
