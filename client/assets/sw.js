var CACHE = 'sam-bowler-v2';
// TODO: Respond with WebP where possible: http://www.deanhume.com/Home/BlogPost/service-workers--dynamic-responsive-images-using-webp-images/10132
var CACHE_ASSETS = [
  'https://fonts.googleapis.com/css?family=Work+Sans:400,500',
  '/css/style.css',
  '/js/bundle.js',
  '/img/contour-pattern-light.svg',
  '/img/my-head-120.jpg',
  '/img/my-head-150.jpg',
  '/img/my-head-160.jpg',
  '/img/my-head-180.jpg',
  '/img/my-head-200.jpg',
  '/img/my-head-240.jpg',
  '/img/my-head-300.jpg',
  '/img/my-head-320.jpg',
  '/img/my-head-360.jpg',
  '/img/my-head-400.jpg',
  '/img/uk-radio-player-interface-250.jpg',
  '/img/uk-radio-player-interface-350.jpg',
  '/img/uk-radio-player-interface-450.jpg',
  '/img/uk-radio-player-interface-500.jpg',
  '/img/uk-radio-player-interface-550.jpg',
  '/img/uk-radio-player-interface-700.jpg',
  '/img/uk-radio-player-interface-900.jpg',
  '/manifest.json'
];

self.addEventListener('install', function(e) {
  e.waitUntil(
    caches.open(CACHE)
      .then(function(cache) {
        return cache.addAll(CACHE_ASSETS);
      })
      .then(self.skipWaiting())
  );
});

// Clean up old caches
self.addEventListener('activate', function(e) {
  e.waitUntil(
    caches.keys()
      .then(function(cacheNames) {
        return cacheNames.filter(function(cacheName) {
          return cacheName !== CACHE;
        })
      })
      .then(function(cachesToDelete) {
        return Promise.all(cachesToDelete.map(function(cacheToDelete) {
          return caches.delete(cacheToDelete);
        }))
      })
      .then(function() {
        self.clients.claim()
      })
  )
});

self.addEventListener('fetch', function(e) {
  // Avoid external requests
  if(e.request.url.startsWith(self.location.origin)) {
    // Respond with cached asset if available
    e.respondWith(
      caches.match(e.request).then(function(response) {
        return response || fetch(e.request);
      })
    );
  }
});
